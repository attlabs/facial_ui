/**
 * Created by jjacobs on 8/26/13.
 */

var scene = (function () {

	if (!Detector.webgl) Detector.addGetWebGLMessage();

	var container;
	var group1, group2, group3, group4, totObjs = [];
	var camera, scene, renderer;
	var objects = [];

	function init() {

		container = document.getElementById('container');

		if (container.hasChildNodes()) {
			while (container.childNodes.length >= 1) {
				container.removeChild(container.firstChild);
			}
		}

		scene = new THREE.Scene();

		camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
		camera.position.z = 300;
		//camera.position.y = -200;
		camera.position.x = -100;
		scene.add(camera);
		controls = new THREE.TrackballControls( camera );

		// controls.rotateSpeed = 1.0;
		controls.zoomSpeed = 1.2;
		controls.panSpeed = 0.8;

		controls.noZoom = false;
		controls.noPan = false;

		controls.staticMoving = true;
		controls.dynamicDampingFactor = 0.3;

		controls.keys = [ 65, 83, 68 ];

		controls.addEventListener( 'change', render );

		var geometry = new THREE.CubeGeometry(80, 80, 80);
//Created a horizontal single group
		group1 = new THREE.Object3D();
		group2 = new THREE.Object3D();
		group3 = new THREE.Object3D();
		group4 = new THREE.Object3D();

		var k = 0;
		var y = 0;

		// WebGL
		for (var i = -7; i < 1; i++) {
			//Specific material image city

			var material = new THREE.MeshLambertMaterial({
				map: THREE.ImageUtils.loadTexture('../images/posters/poster' + k + '.jpg'), overdraw: true
			});

			var mesh = new THREE.Mesh(geometry, material);
			mesh.overdraw = true;

			mesh.position.x = i * 60 + y;
			mesh.position.y = -150;
			mesh.position.z = 0;

			mesh.matrixAutoUpdate = false;
			mesh.updateMatrix();
			group1.add(mesh);
			objects.push(mesh);
			totObjs.push(objects);
			y += 60;
			if (k == 9)
				k = 0;
			else
				k++;

		}
		y = 0;
		for (var i = -7; i < 1; i++) {
			var material = new THREE.MeshLambertMaterial({
				map: THREE.ImageUtils.loadTexture('../images/posters/poster' + k + '.jpg'), overdraw: true
			});

			var mesh = new THREE.Mesh(geometry, material);
			mesh.overdraw = true;

			mesh.position.x = i * 60 + y;
			mesh.position.y = -40;
			mesh.position.z = 0;
			mesh.matrixAutoUpdate = false;
			mesh.updateMatrix();
			group2.add(mesh);
			objects.push(mesh);
			y += 60;
			if (k == 9)
				k = 0;
			else
				k++;

		}
		y = 0;
		for (var i = -7; i < 1; i++) {
			var material = new THREE.MeshLambertMaterial({
				map: THREE.ImageUtils.loadTexture('../images/posters/poster' + k + '.jpg'), overdraw: true
			});

			var mesh = new THREE.Mesh(geometry, material);
			mesh.overdraw = true;

			mesh.position.x = i * 60 + y;
			mesh.position.y = 60;

			mesh.matrixAutoUpdate = false;
			mesh.updateMatrix();
			group3.add(mesh);
			objects.push(mesh);
			y += 60;
			if (k == 9)
				k = 0;
			else
				k++;

		}
		y = 0;
		for (var i = -7; i < 1; i++) {
			var material = new THREE.MeshLambertMaterial({
				map: THREE.ImageUtils.loadTexture('../images/posters/poster' + k + '.jpg'), overdraw: true
			});

			var mesh = new THREE.Mesh(geometry, material);
			mesh.overdraw = true;

			mesh.position.x = i * 60 + y;
			mesh.position.y = 170;
			mesh.position.z = 0;
			mesh.matrixAutoUpdate = false;
			mesh.updateMatrix();
			group4.add(mesh);
			objects.push(mesh);
			y += 60;
			if (k == 9)
				k = 0;
			else
				k++;
		}

		//Upload group into scene
		scene.add(group1);
		scene.add(group2);
		scene.add(group3);
		scene.add(group4);

		var light = new THREE.SpotLight(0xffffff, 1.2);
		light.position.set(0, 500, 1000);
		light.castShadow = true;
		scene.add(light);

		scene.add(new THREE.AmbientLight(0x505050));

		try {
			renderer = new THREE.WebGLRenderer();
			renderer.setSize($('#container').width(), $(window).height());
//				        renderer.shadowMapEnabled = true;
//				        renderer.shadowMapSoft = true;
//				        renderer.domElement.style.position = "relative";

			container.appendChild(renderer.domElement);
		}
		catch (e) {
			console.log(e);
		}

	}

	function animate() {
		requestAnimationFrame(animate);
		render();
		controls.update();
	}

	function render() {
		renderer.render(scene, camera);
	}

	return {
		init : function() {
			init();
			animate();
		}
	}
})();